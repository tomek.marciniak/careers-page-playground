import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron, CronExpression } from '@nestjs/schedule';
import {
  News,
  PresskitApiResponse,
  PresskitApiResponseNews,
} from './news.types';

const NEWS_LIMIT = 10;

@Injectable()
export class NewsService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
  ) {
    this.fetchAndStoreData();
  }
  private readonly logger = new Logger(NewsService.name);
  private latestNewsPromise: Promise<News[]>;
  private latestNews: News[] = [];

  // In real life app we'll probably want to do it every hour or so.
  // Ideally we could expose an endpoint and connect it to Greenhouse Webhook
  // So the data is updated whenever it changes in the Greenhouse
  @Cron(CronExpression.EVERY_30_SECONDS)
  handleCron() {
    this.logger.log('CRON Scheduler triggered every 30 seconds');
    this.fetchAndStoreData();
  }

  transformData(allNewsData: PresskitApiResponseNews[]) {
    return allNewsData.map(({ title, logo, description, link, link_url }) => ({
      title,
      logo,
      description,
      link: {
        label: link,
        url: link_url,
      },
    }));
  }

  fetchAndStoreData(): void {
    this.logger.log('Fetching data from Picsart Presskit');

    const baseURL = this.configService.get('presskit.url');
    const apiKey = this.configService.get('presskit.key');
    const url = `${baseURL}?access_token=${apiKey}`;

    // @ Todo: error handling
    this.latestNewsPromise = new Promise((resolve, reject) => {
      this.httpService.get<PresskitApiResponse>(url).subscribe((response) => {
        const { data } = response;
        this.logger.log(`Received data from Presskit (${data.count} news).`);
        this.latestNews = this.transformData(data.data).slice(0, NEWS_LIMIT);
        resolve(this.latestNews);
      });
    });
  }

  async getLatestNews() {
    if (this.latestNews.length) {
      return this.latestNews;
    }
    return await this.latestNewsPromise;
  }
}
