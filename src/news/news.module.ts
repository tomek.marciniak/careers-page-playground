import { Module, CacheModule } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';

@Module({
  imports: [HttpModule, CacheModule.register()],
  controllers: [NewsController],
  providers: [NewsService],
})
export class NewsModule {}
