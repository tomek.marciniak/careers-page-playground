export interface News {
  title: string;
  logo: string;
  description: string;
  link: NewsLink;
}

export interface NewsLink {
  label: string;
  url: string;
}

export interface PresskitApiResponse {
  status: string;
  count: number;
  data: PresskitApiResponseNews[];
}

export interface PresskitApiResponseNews {
  score: number;
  _id: string;
  created: string;
  published_date: string;
  title: string;
  logo: string;
  link: string;
  link_url: string;
  description: string;
  type: string;
  updated: string;
  __v: number;
}
