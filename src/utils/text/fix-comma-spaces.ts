const FIX_COMMAS_REGEXP = /\,(\S)/g;

// comma,next word -> comma, next word
const fixCommaSpaces = (text: string) =>
  text.replace(FIX_COMMAS_REGEXP, `, $1`);

export default fixCommaSpaces;
