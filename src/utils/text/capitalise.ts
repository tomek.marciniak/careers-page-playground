const CAPITALISE_REGEXP = /(^\w|\s\w)/g;

// some Example sentence -> Some Example Sentence
const capitalise = (text = '') =>
  text.replace(CAPITALISE_REGEXP, (char) => char.toUpperCase());

export default capitalise;
