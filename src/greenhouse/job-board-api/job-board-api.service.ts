import {
  CACHE_MANAGER,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { Observable, catchError, map, of } from 'rxjs';
import { ConfigService } from '@nestjs/config';
import uniques from 'src/utils/array/uniques';

const TIMEOUT = 5000;
const CACHE_TTL = 15;
// const CACHE_TTL = 60 * 30;

@Injectable()
export class JobBoardApiService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    @Inject(CACHE_MANAGER) protected readonly cacheManager,
  ) {}
  /* 
    This isn't necessarily what we want in real service, but the idea here is
    that we can extract data for different endpoints from the whole list
    and the list shouldn't be changing very often, so it makes no sense to 
    refetch the list all the time.
  */
  private fetchJobs() {
    const cacheKey = 'jobs';
    const jobsUrl = `${this.configService.get('greenhouse.jobBoard.url')}/jobs`;

    return new Observable((subscriber) => {
      this.cacheManager.get(cacheKey).then((cachedData) => {
        if (cachedData) {
          subscriber.next(cachedData);
          subscriber.complete();
        } else {
          this.httpService
            .get(jobsUrl, {
              timeout: TIMEOUT,
              params: {
                content: true,
              },
            })
            .subscribe((data) => {
              this.cacheManager.set(cacheKey, data, { ttl: CACHE_TTL });
              subscriber.next(data);
              subscriber.complete();
            });
        }
      });
    });
  }
  private handleError({ code, message }) {
    return of({ code, message });
  }

  private extractListData(jobData) {
    if (!jobData) {
      throw new NotFoundException();
    }

    const { id, title, departments, location } = jobData;

    return {
      id,
      title,
      location: location?.name,
      departments: departments.map((department) => department?.name),
    };
  }

  private extractDetailsData(jobData) {
    if (!jobData) {
      throw new NotFoundException();
    }

    const { offices, content } = jobData;

    return {
      ...this.extractListData(jobData),
      content,
      offices: offices.map((office) => office?.name),
    };
  }

  getList() {
    return this.fetchJobs().pipe(
      map(({ data }) => data?.jobs.map(this.extractListData)),
      catchError(this.handleError),
    );
  }

  getOne(id: number) {
    return this.fetchJobs().pipe(
      map(({ data }) => data?.jobs.find((job) => job.id === id)),
      map(this.extractDetailsData),
      catchError(this.handleError),
    );
  }

  getLocations() {
    return this.getList().pipe(
      map((data) => {
        const locations = data.map(({ location }) => location);
        return uniques(locations);
      }),
    );
  }

  getDepartments() {
    return this.getList().pipe(
      map((data) => {
        const departments = data.map(({ departments }) => departments).flat();
        return uniques(departments);
      }),
    );
  }

  getPositions() {
    return this.getList().pipe(
      map((data) => {
        const positions = data.map(({ title }) => title);
        return uniques(positions);
      }),
    );
  }
}
