import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { zip } from 'rxjs';
import { HarvestJobPost, HarvestJob } from './greenhouse.types';
import { HarvestApiService } from './harvest-api/harvest-api.service';
import { JobDetails } from '../jobs/jobs.types';
import capitalise from '../utils/text/capitalise';
import fixCommaSpaces from '../utils/text/fix-comma-spaces';

@Injectable()
export class GreenhouseService {
  constructor(private harvestAPI: HarvestApiService) {
    this.fetchAndStoreData();
  }
  private readonly logger = new Logger(GreenhouseService.name);
  private availableJobsPromise: Promise<JobDetails[]>;
  private availableJobs: JobDetails[] = [];

  // In real life app we'll probably want to do it every hour or so.
  // Ideally we could expose an endpoint and connect it to Greenhouse Webhook
  // So the data is updated whenever it changes in the Greenhouse
  // ---
  // If we want to keep the timeout in the config, we need to do it this way
  // https://stackoverflow.com/questions/69463692/nestjs-using-environment-configuration-on-cron-decorator
  // ---
  @Cron(CronExpression.EVERY_30_SECONDS)
  handleCron() {
    this.logger.log('CRON Scheduler triggered every 30 seconds');
    this.fetchAndStoreData();
  }

  convertToDictionary(jobsMetadata: HarvestJob[]): {
    [id: string]: HarvestJob;
  } {
    return jobsMetadata.reduce((result, current: HarvestJob) => {
      result[current.id] = current;
      return result;
    }, {});
  }

  extractCustomFields(job?: HarvestJob) {
    // @Todo: Decide on conventions, figure out renaming
    return {
      division: job?.custom_fields?.division,
      subdivision: job?.custom_fields?.['*subdivision'],
      employmentType: fixCommaSpaces(
        capitalise(job?.custom_fields?.['employment_type']?.replace('-', ' ')),
      ),
    };
  }

  transformData(
    jobPosts: HarvestJobPost[],
    jobsMetadata: HarvestJob[],
  ): JobDetails[] {
    const jobsDictionary = this.convertToDictionary(jobsMetadata);

    return jobPosts.map((jobPost) => {
      return {
        id: jobPost.id,
        title: jobPost.title,
        location: jobPost.location?.name,
        content: jobPost.content,
        timestamp: new Date(jobPost.first_published_at).getTime(),
        questions: jobPost.questions?.map(
          ({ required, label, name, type }) => ({
            required,
            label,
            name,
            type,
          }),
        ),
        ...this.extractCustomFields(jobsDictionary[jobPost.job_id]),
      };
    });
  }

  fetchAndStoreData(): void {
    this.logger.log('Fetching data from Greenhouse');

    // @Todo: error handling
    this.availableJobsPromise = new Promise((resolve) => {
      zip(this.harvestAPI.getJobPosts(), this.harvestAPI.getJobs()).subscribe(
        ([jobPosts, jobsMetadata]) => {
          this.logger.log(
            `Received data from Greenhouse (${jobPosts.length} job posts, ${jobsMetadata.length} jobs).`,
          );
          this.availableJobs = this.transformData(jobPosts, jobsMetadata);
          resolve(this.availableJobs);
        },
      );
    });
  }

  async getAvailableJobs() {
    if (this.availableJobs.length) {
      return this.availableJobs;
    }
    return await this.availableJobsPromise;
  }
}
