import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { map, switchMap, of, Observable } from 'rxjs';
import { HarvestJob, HarvestJobPost } from '../greenhouse.types';

const PER_PAGE = 50;

@Injectable()
export class HarvestApiService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
  ) {}
  private readonly logger = new Logger(HarvestApiService.name);

  getRequestParams(customQuery = {}) {
    return {
      auth: {
        username: this.configService.get('greenhouse.harvest.key'),
        password: '',
      },
      params: {
        per_page: PER_PAGE,
        ...customQuery,
      },
    };
  }

  fetchAllPages(url: string, requestParams: any) {
    const fetchPage = (page: number, previousData: []) =>
      this.httpService
        .get(url, this.getRequestParams({ ...requestParams, page }))
        .pipe(
          switchMap((response) => {
            if (response.headers.link.includes('rel="next"')) {
              return fetchPage(page + 1, response.data).pipe(
                map((currentPageData: []) => [
                  ...currentPageData,
                  ...previousData,
                ]),
              );
            } else {
              return of([...response.data, ...previousData]);
            }
          }),
        );

    return fetchPage(1, []);
  }

  // Job posts are what we want to display on the page, but we need to decorate it with data
  // from the jobs endpoint.
  getJobPosts(): Observable<HarvestJobPost[]> {
    const jobPostsUrl = `${this.configService.get(
      'greenhouse.harvest.url',
    )}/job_posts`;

    return this.fetchAllPages(jobPostsUrl, {
      live: true,
      active: true,
    });
  }

  // Some data isn't available on the job post level, so we need to extract it from job.
  getJobs(): Observable<HarvestJob[]> {
    const jobsUrl = `${this.configService.get('greenhouse.harvest.url')}/jobs`;

    return this.fetchAllPages(jobsUrl, {
      status: 'open',
    });
  }
}
