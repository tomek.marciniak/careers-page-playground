import { CacheModule, Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { HarvestApiService } from './harvest-api/harvest-api.service';
import { JobBoardApiService } from './job-board-api/job-board-api.service';
import { GreenhouseService } from './greenhouse.service';

@Module({
  imports: [HttpModule, CacheModule.register()],
  exports: [JobBoardApiService, HarvestApiService, GreenhouseService], // Just for the data explorer, not for real app
  providers: [HarvestApiService, JobBoardApiService, GreenhouseService],
})
export class GreenhouseModule {}
