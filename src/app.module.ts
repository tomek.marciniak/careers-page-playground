import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { JobsModule } from './jobs/jobs.module';
import { DataExplorerModule } from './data-explorer/data-explorer.module';
import config from './config/config';
import { NewsModule } from './news/news.module';

@Module({
  imports: [
    JobsModule,
    NewsModule,
    DataExplorerModule,
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
  ],
})
export class AppModule {}
