export default () => ({
  greenhouse: {
    // Job Board API
    jobBoard: {
      url:
        process.env.GREENHOUSE_JOB_BOARD_URL ||
        'https://boards-api.greenhouse.io/v1/boards/picsart',
      key: process.env.GREENHOUSE_JOB_BOARD_KEY,
    },
    harvest: {
      url:
        process.env.GREENHOUSE_HARVEST_URL ||
        'https://harvest.greenhouse.io/v1',
      key: process.env.GREENHOUSE_HARVEST_KEY,
    },
  },
  // Presskit API
  presskit: {
    url: process.env.PRESSKIT_URL || 'https://api.picsart.com/presskit',
    key: process.env.PRESSKIT_KEY,
  },
});
