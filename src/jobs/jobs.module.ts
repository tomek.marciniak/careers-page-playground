import { Module, CacheModule } from '@nestjs/common';
import { JobsService } from './jobs.service';
import { JobsController } from './jobs.controller';
import { GreenhouseModule } from '../greenhouse/greenhouse.module';
@Module({
  imports: [CacheModule.register(), GreenhouseModule],
  controllers: [JobsController],
  providers: [JobsService],
})
export class JobsModule {}
