import { Controller, Get, Param } from '@nestjs/common';
import { JobsService } from './jobs.service';

@Controller('jobs')
export class JobsController {
  constructor(private readonly jobsService: JobsService) {}
  @Get('/')
  getList() {
    return this.jobsService.getJobsList();
  }
  @Get('/filters')
  getFilters() {
    return this.jobsService.getFilters();
  }
  @Get('/:id')
  getJobDetails(@Param('id') id: string) {
    return this.jobsService.getJobDetails(+id);
  }
}
