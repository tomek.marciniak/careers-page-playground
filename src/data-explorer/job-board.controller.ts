import { Controller, Get, Param } from '@nestjs/common';
import { JobBoardApiService } from '../greenhouse/job-board-api/job-board-api.service';

@Controller('explore/job-board')
export class JobBoardController {
  constructor(private readonly jobBoardApiService: JobBoardApiService) {}
  @Get('/jobs')
  getJobList() {
    return this.jobBoardApiService.getList();
  }

  @Get('/jobs:id')
  getJob(@Param('id') id: string) {
    return this.jobBoardApiService.getOne(+id);
  }

  @Get('/locations')
  getLocations() {
    return this.jobBoardApiService.getLocations();
  }

  @Get('/departments')
  getDepartments() {
    return this.jobBoardApiService.getDepartments();
  }

  @Get('/positions')
  getPositions() {
    return this.jobBoardApiService.getPositions();
  }
}
