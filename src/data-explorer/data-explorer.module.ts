import { Module } from '@nestjs/common';
import { GreenhouseModule } from '../greenhouse/greenhouse.module';
import { HarvestController } from './harvest.controller';
import { JobBoardController } from './job-board.controller';

@Module({
  controllers: [JobBoardController, HarvestController],
  imports: [GreenhouseModule],
})
export class DataExplorerModule {}
