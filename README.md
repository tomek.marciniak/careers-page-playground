## Description

Playground for the careers page rewrite.
It's rough and dirty so should be only used for experimenting.
Data explorer module is used for experimenting with job boards api and playing with data.
Jobs Module is used to experiment with target API that we'd like to design for the real service.

# Config

- To use Harvest API an environment variable GREENHOUSE_HARVEST_KEY needs to be set when starting the app.
- To use Picsart API to fetch the news environment variable PRESSKIT_KEY needs to be set when starting the app.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

# Actual API PoC

- http://localhost:3000/jobs List all the jobs (short summary)
- http://localhost:3000/jobs/:id Get single job details (full data)
- http://localhost:3000/jobs/filters Get filters
- http://localhost:3000/news - Get 10 latest news

## Exploration endpoints available

We can browse the available data using endpoints below.
This is not supposed to be the final API shape, just a tool to get familiar with NestJS and the Greenhouse data.

# Job Board API

- http://localhost:3000/explore/job-board/jobs
- http://localhost:3000/explore/job-board/jobs/:id
- http://localhost:3000/explore/job-board/jobs/locations
- http://localhost:3000/explore/job-board/jobs/positions
- http://localhost:3000/explore/job-board/jobs/departments

# Harvest API

- http://localhost:3000/explore/harvest/jobs
